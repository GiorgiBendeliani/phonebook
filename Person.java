package phonebook1;

import java.time.LocalDate;

//Person class which will save basic info about a Person

public class Person implements Comparable<Person>{
	private String name;
	
	private String lastName;
	
	private LocalDate dateOfBirth;
	
	private String phoneNumber;

	public Person(String name, String lastName, LocalDate dob, String phoneNumber) {
		this.name = name;
		this.lastName = lastName;
		this.dateOfBirth = dob;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth + ", phoneNumber="
				+ phoneNumber + "]";
	}

	@Override
	public int compareTo(Person o) {
		return this.lastName.compareTo(o.lastName);
	}
}
