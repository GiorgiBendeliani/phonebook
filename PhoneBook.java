package phonebook1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class PhoneBook {

    private static List<Person> persons = new ArrayList<>();

//    static {
//        persons.add(new Person("Jan", "Kowalski", LocalDate.of(1990, 12, 30), "586914586"));
//        persons.add(new Person("Anna", "Nowak", LocalDate.of(1989, 6, 3), "123456789"));
//        persons.add(new Person("Monika", "Kwasniewska", LocalDate.of(1980, 5, 25), "658912362"));
//        persons.add(new Person("Slawek", "Grzesiak", LocalDate.of(1980, 6, 25), "658912362"));
//        persons.add(new Person("Marcin", "Dyzma", LocalDate.of(1967, 10, 18), "658912362"));
//        persons.add(new Person("Kasia", "Browiec", LocalDate.of(1981, 9, 25), "658912362"));
//        persons.add(new Person("Tomek", "Nurek", LocalDate.of(1985, 4, 11), "658912362"));
//        persons.add(new Person("Dorota", "Borowik", LocalDate.of(1912, 5, 25), "658912362"));
//        persons.add(new Person("Ania", "Duszek", LocalDate.of(1953, 9, 24), "658912362"));
//    }
    
    

    public static void main(String[] args) throws IOException {
    	
    	
    	System.out.println(persons);
   	 	
   	 	
   	 	do {
   	 		showMenu();
   	 		int value = Klawiatura.readInt();
  	 	
   	 		
   	 		switch (value) {
   	 	
		case 1:
			addPerson();
			persons.stream().forEach(System.out::println);

			break;
		case 4: 
			writeToFile();
			
			break;
		case 2:
			loadList();
			break;
		case 3: 
			System.out.println(persons);
			break;
		case 5: 
			System.exit(0);
			break;
		default:
			System.out.println("Nie wiem co zrobic...");
			break;
		}
   	 	
   	 	} while (true);
   	 	
   	 	
   	
    }

	private static void writeToFile() {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("Contacts.txt");
			for(Person p : persons){
//				writer.append(p.getName() + ", " +  p.getLastName() + ", " + p.getPhoneNumber());
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(p.getName()).append(", ").append(p.getLastName()).append(", ")
				.append(p.getDateOfBirth().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).append(", ").append(p.getPhoneNumber());
			
				writer.println(stringBuilder.toString());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(writer!=null){
				writer.close();
			}
		}
		
	
}

	private static void addPerson() {
		System.out.println("Podaj imie: ");
		String name = Klawiatura.readLine();
		System.out.println("Podaj nazwisko: ");
		String lastName = Klawiatura.readLine();
		System.out.println("Podaj numer telefonu: ");
		String phoneNumber = Klawiatura.readLine();
		
		System.out.println("Podaj date urodzenia (Rok - mesi�c - dzien): ");
		String dateOfBirth = Klawiatura.readLine();
		
		LocalDate dob = null;
		try {
			dob = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			persons.add(new Person(name, lastName, dob, phoneNumber));
		} catch (java.time.format.DateTimeParseException exc){
			System.out.println("Niepoprawny format daty");
			
		}
		
		
		
	}
	
	private static void loadList(){
		try(BufferedReader reader = new BufferedReader(new FileReader("Contacts.txt"))) {
			String line = null;
			while ((line = reader.readLine()) != null){
				String[] splitArray = line.split(",");
				System.out.println(splitArray[1]);
		}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void showMenu() {
	System.out.println("######MENU########");
	System.out.println("Dzie� dobry, wybierz opcje z menu: ");
	System.out.println("1. Dodaj osobe "); 
	System.out.println("2. Wyswietl liste "); 
	System.out.println("3. Wczytaj z pliku ");  
	System.out.println("4. Zapisz");
}

}